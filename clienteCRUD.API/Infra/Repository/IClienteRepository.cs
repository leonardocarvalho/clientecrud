﻿using clienteCRUD.API.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace clienteCRUD.API.Infra.Repository
{
    public interface IClienteRepository
    {
        Task<List<Cliente>> GetAll();
        Task<Cliente> Get(Guid id);
        Task<bool> Save(Cliente cliente);
        Task<bool> Update(Cliente cliente);
        Task<bool> Delete(Cliente cliente);
    }
}
