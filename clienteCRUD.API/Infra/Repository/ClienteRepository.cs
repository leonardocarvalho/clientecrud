﻿using clienteCRUD.API.Domain.Entities;
using clienteCRUD.API.Infra.Database;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace clienteCRUD.API.Infra.Repository
{
    public class ClienteRepository : IClienteRepository
    {
        private readonly DatabaseContext _dbContext;

        public ClienteRepository(DatabaseContext dbContext)
        {
            _dbContext = dbContext;
        }

        public async Task<List<Cliente>> GetAll()
        {
            return _dbContext.Cliente.ToList();
        }

        public async Task<Cliente> Get(Guid id)
        {
            return _dbContext.Cliente.FirstOrDefault(x => x.Id == id);
        }

        public async Task<bool> Save(Cliente cliente)
        {

            _dbContext.Cliente.Add(cliente);
            return (_dbContext.SaveChanges() > 0);

        }
        public async Task<bool> Update(Cliente cliente)
        {
            _dbContext.Cliente.Update(cliente);
            return (await _dbContext.SaveChangesAsync() > 0);
        }

        public async Task<bool> Delete(Cliente cliente)
        {
            _dbContext.Cliente.Remove(cliente);
            return (await _dbContext.SaveChangesAsync() > 0);
        }
    }
}
