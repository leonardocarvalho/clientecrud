﻿using AutoMapper;
using clienteCRUD.API.Domain.Entities;
using clienteCRUD.API.Domain.Service;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace clienteCRUD.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ClienteController : Controller
    {
        public readonly IClienteService _clienteService;

        public ClienteController(IClienteService clienteService)
        {
            _clienteService = clienteService;
        }

        [HttpGet]
        public async Task<IActionResult> Get()
        {

            var response = await _clienteService.GetAll();
            return Ok(response);

        }

        [HttpGet("{id}")]
        public async Task<IActionResult> Get(Guid id)
        {
            var response = await _clienteService.Get(id);
            if (response != null)
                return Ok(response);

            return BadRequest();
        }

        [HttpPost]
        public async Task<IActionResult> Post([FromBody] Cliente cliente)
        {
            var response = await _clienteService.Save(cliente);
            if (response)
                return Ok(response);

            return BadRequest(response);
        }

        [HttpPut]
        public async Task<IActionResult> Put([FromBody] Cliente cliente)
        {
            var response = await _clienteService.Update(cliente);
            if (response)
                return Ok(response);

            return BadRequest(response);
        }

        [HttpDelete("{id}")]
        public async Task<IActionResult> Delete(Guid id)
        {
            var response = await _clienteService.Delete(id);
            if (response)
                return Ok(response);

            return BadRequest(response);
        }
    }
}
