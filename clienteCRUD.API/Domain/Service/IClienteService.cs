﻿using clienteCRUD.API.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace clienteCRUD.API.Domain.Service
{
    public interface IClienteService
    {
        Task<List<Cliente>> GetAll();
        Task<Cliente> Get(Guid id);
        Task<bool> Save(Cliente cliente);
        Task<bool> Update(Cliente cliente);
        Task<bool> Delete(Guid id);
    }
}
