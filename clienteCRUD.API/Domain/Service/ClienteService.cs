﻿using clienteCRUD.API.Domain.Entities;
using clienteCRUD.API.Infra.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace clienteCRUD.API.Domain.Service
{
    public class ClienteService : IClienteService
    {
        private readonly IClienteRepository _clienteRepository;

        public ClienteService(IClienteRepository clienteRepository)
        {
            _clienteRepository = clienteRepository;
        }

        public async Task<List<Cliente>> GetAll()
        {
            return await _clienteRepository.GetAll();
        }

        public async Task<Cliente> Get(Guid id)
        {
            return await _clienteRepository.Get(id);
        }

        public async Task<bool> Save(Cliente cliente)
        {
            cliente.Id = Guid.NewGuid();
            return await _clienteRepository.Save(cliente);
        }
        public async Task<bool> Update(Cliente cliente)
        {
            var clienteParaAtualizar = await _clienteRepository.Get(cliente.Id);

            if (clienteParaAtualizar == null)
                return false;

            clienteParaAtualizar.Idade = cliente.Idade;
            clienteParaAtualizar.Nome = cliente.Nome;

            return await _clienteRepository.Update(clienteParaAtualizar);
        }

        public async Task<bool> Delete(Guid id)
        {
            var cliente = await _clienteRepository.Get(id);

            if (cliente == null)
                return false;

            return await _clienteRepository.Delete(cliente);
        }

    }
}
