﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace clienteCRUD.API.Domain.Entities
{
    public class Cliente
    {
        public Guid Id { get; set; }
        [MaxLength(100, ErrorMessage ="Nome deve possuir no máximo 100 caracteres")]
        public string Nome { get; set; }
        [Range(0,100,ErrorMessage ="Idade mínima 0 e máxima 100")]
        public int Idade { get; set; }
    }
}
